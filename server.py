# -*- coding: utf-8 -*-

from bot import telegram_chatbot
import feedparser
from datetime import datetime, timedelta
import datetime
import requests
import re


bot = telegram_chatbot("config.cfg")


def make_pretty(msg):
    pretty_msg = ""
    for key in msg.keys():
        pretty_msg += "\n\nTitle: %s\nPrice:%s\nLink: %s" % (
            msg[key]['title'], msg[key]['price'], msg[key]['link'])
    return pretty_msg


def make_reply(msg):

    msg_list = msg.split('\n')

    if len(msg_list) > 4:
        funda_city = str(msg_list[0])
        funda_price = str(msg_list[1])
        funda_type = str(msg_list[2]).lower()
        funda_rooms = str(msg_list[3])
        funda_square_meter = str(msg_list[4])
        funda_since_date = datetime.date.today()
    else:
        if msg.lower() == 'boxtel':
            funda_price = '250000-375000'  # this is the price range you want
            funda_city = msg  # this can be any city
            funda_type = 'koop'  # 'huur' > rent || 'koop' > buy
            funda_rooms = '3+kamers'  # the amount of rooms you want, eg. 3+kamers or 7+kamers
            # the amount of square meter you need, eq. 125+woonoop is 125m2
            funda_square_meter = '100+woonopp'
            funda_since_date = datetime.date.today()
        else:
             return "Request failed, please insert your request in the following format: \n City \n price range (200000-290000)\n type(Koop of Huur) \n rooms (3+kamers)\n Square meters (100+woonopp)"
        

    funda_url = "http://partnerapi.funda.nl/feeds/Aanbod.svc/rss/?type=" + funda_type + "&zo=/" + funda_city + \
        "/" + funda_price + "/" + funda_rooms + "/" + \
        funda_square_meter + "&since=" + str(funda_since_date)

    headers = []
    try:
        web_page = requests.get(
            funda_url, headers=headers, allow_redirects=True)
        content = web_page.content.strip()  # drop the first newline (if any)
        rss_feed = feedparser.parse(content)

        # print(rss_feed)

        collection_array = {}

        for entry in rss_feed.entries:
            address = entry.title.replace("te koop:", " ")
            address = re.sub(r'\s?\+\s+PP\s?', '', address)
            collection_array[entry.title] = {
                'title': entry.title,
                'link': entry.link,
                # 'description': entry.description
                'price': (re.findall(r'<span class="price">(.*?)</span>',
                                     entry.description)[0].replace('.', '').replace('\xa0', ''))
            }
        return make_pretty(collection_array)
    except:
        return "Request failed, please insert your request in the following format: \n City (Boxtel) \n price range (200000-290000)\n type(koop of huur) \n rooms (3+kamers)\n Square meters (100+woonopp)"
        


update_id = None
while True:
    updates = bot.get_updates(offset=update_id)
    updates = updates["result"]
    if updates:
        for item in updates:
            update_id = item["update_id"]
            if 'message' in item:
                if not 'text' in item['message']:
                    print("try again")
                    continue
                try:
                    message = str(item["message"]["text"])
                except:
                    message = None
                from_ = item["message"]["from"]["id"]
                reply = make_reply(message)
                bot.send_message(reply, from_)
            else:
                print("Update without message found")
