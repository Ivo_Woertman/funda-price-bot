import urllib3
import feedparser
from datetime import datetime, timedelta
import datetime
import requests
import re

class telegram_funda():

    def load_Funda(self):
        funda_price = '275000-375000' # this is the price range you want
        funda_city = 'Boxtel' # this can be any city
        funda_type = 'koop' # 'huur' > rent || 'koop' > buy
        funda_rooms = '3+kamers' # the amount of rooms you want, eg. 3+kamers or 7+kamers
        funda_square_meter = '75+woonopp' # the amount of square meter you need, eq. 125+woonoop is 125m2
        funda_since_date = datetime.date.today()

        funda_url = "http://partnerapi.funda.nl/feeds/Aanbod.svc/rss/?type=" + funda_type + "&zo=/" + funda_city + "/" + funda_price + "/" + funda_rooms + "/" + funda_square_meter + "&since=" + str(funda_since_date)

        headers = []
        web_page = requests.get(funda_url, headers=headers, allow_redirects=True)
        content = web_page.content.strip()  # drop the first newline (if any)
        rss_feed = feedparser.parse(content)

        # print(rss_feed)

        collection_array = {}
        for entry in rss_feed.entries:
            address = entry.title.replace("te koop:", " ")
            address = re.sub(r'\s?\+\s+PP\s?', '', address)       
            collection_array[entry.title] = {
                        'title': entry.title,
                        'link': entry.link
                        # 'description': entry.description
                    }

        return collection_array



# NewsFeed = feedparser.parse("https://timesofindia.indiatimes.com/rssfeedstopstories.cms")
# entry = NewsFeed.entries[1]
# print(entry.keys())






#(Time.now - 1.days).strftime('%Y%m%d') # the result based on date
